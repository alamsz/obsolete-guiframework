Feature: Open login page, test validation is working and do successful login

	Scenario: Verify the Login Page is displayed properly
		Given I am ON "LoginPage"
		Then I should EXPECT
			| LoginPage.username	| visible |
			| LoginPage.password	| visible |
			| LoginPage.loginButton	| visible |

	Scenario: Simulate Login fail, verification should be working
		Given I am ON "LoginPage"
		When I click "loginButton"
		Then I should EXPECT 
			| LoginPage.invalidLogonCredentialMessage | visible |
	
	@last
	Scenario: Simulate successful Login
		Given I am ON "LoginPage"
		When I enter
			| LoginPage.username	| ADMIN 	|
			| LoginPage.password	| yambay 	|
		And I click "loginButton"
		Then I wait 5 seconds
		And I should be ON "HomePage"			