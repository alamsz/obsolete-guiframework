Feature: Building GUI test automation job 

Scenario: Accessing Jenkins 
    Given I am ON "HomePage" 
    When I click "Mitrais GUI Automation Framework Job link" 
    Then I should be ON "GUIAUtomationJobPage" 
    
@last 
Scenario: Build the Job 
    Given I am ON "GUIAUtomationJobPage" 
    When I click "Build Now Link" 
    Then I should EXPECT 
        |Build Now Link|visible|
