Feature: Open setting, try click all possible button

  Scenario: Login in
    Given I have logged in using
      | username | olivia.charlotte                 |
      | email    | oliviacharlotte.doctor@gmail.com |
      | password | password                         |

  Scenario: Go to Setting Page
    Given I am ON "HomePage"
    And I click "more"
    And I click "setting"
    Then I should be ON "SettingPage"

  Scenario: clicking subscription detail button
    Given I am ON "SettingPage"
    And I click "subscriptionDetails"
    Then I should be ON "SubscriptionPage"

  @last
  Scenario: back to setting page
    Given I am ON "SubscriptionPage"
    And I click "back"
    Then I should be ON "SettingPage"
