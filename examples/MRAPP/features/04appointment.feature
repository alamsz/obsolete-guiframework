Feature: Simulate Create appointment, drag appointment, and delete appointment 

Scenario: Login in 
    Given I have logged in using 
        | username | olivia.charlotte                 |
        | email    | oliviacharlotte.doctor@gmail.com |
        | password | password                         |
        
Scenario: Clicking appointment menu 
    Given I am ON "HomePage" 
    When I click "appointment" 
    Then I should be ON "Appointment Page" 
    
Scenario: Create new appointment 
    Given I am ON "Appointment Page" 
    When I click "new appointment button" 
    And I enter 
        | patientName | seb |
    And I click "firstResultofDropdown" 
    And I select dropdown 
        | urgency      | Urgent           |
        | provider     | Olivia Charlotte |
        | type-minutes | 120 Min          |
        | room         | Room 1           |
        | type         | Operation        |
    And I pick datetime 
        | time | 19/01/2015 10:00 AM |		
    And I click "save appointment" 
    Then I should EXPECT 
        | newAppointment | VISIBLE |
        
Scenario: Drag newly created appointment to another provider 
    Given I am ON "Appointment Page" 
    When I drag "newAppointment" to "targetPlace" 
    And I double-click ON "newAppointment" 
    Then I should EXPECT 
        | providerselected | Zoe Sophia |
        
@last 
Scenario: Delete newly created appointment 
    Given I am ON "Appointment Page" 
    And I have clicked "cancelPopup" 
    When I delete "newAppointment" 
    And I click "confirmDelete" 
    
    #@last 
    #Scenario: Simulate Logout 
    #    Given I am ON "Menu Page" 
    #    When I click "account menu" 
    #    And I click "logout" 
    #    Then I should be ON "Login Page"