Feature: Open login page, test validation is working, and do successful login 

@last 
Scenario: Simulate successful login 
    Given  I am ON "LoginPage" 
    When  I enter 
        | LoginPage.username | olivia.charlotte                 |
        | LoginPage.email    | oliviacharlotte.doctor@gmail.com |
        | LoginPage.password | password                         |
    And  I click "loginButton"