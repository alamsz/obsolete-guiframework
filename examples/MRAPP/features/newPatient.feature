Feature: Patient page test, create new patient

  Scenario: Login in
    Given I have logged in using
      | username | olivia.charlotte                 |
      | email    | oliviacharlotte.doctor@gmail.com |
      | password | password                         |

  Scenario: navigate to patient page
    Given I am ON "HomePage"
    When I click "patients"
    Then I should be ON "PatientsPage"

  Scenario: open new patient form
    Given I am ON "PatientsPage"
    When I click "newPatient"
    Then I should be ON "New Patient Page"

  Scenario: fill details form
    Given I am ON "New Patient Page"
    When I enter
      | patient detail first name           | Aldira1   |
      | patient detail middle name          | Putra     |
      | patient detail last name            | Raharja   |
      | patient detail prefered name        | Dira      |
      | patient detail primary address 1    | Sidney    |
      | patient detail primary post code    | 1230      |
      | patient detail primary home phone   | 0818000   |
      | patient detail primary mobile phone | 081800081 |
    And I pick date
      | patient detail DOB | 17/11/1992 |
    And I select dropdown
      | patient detail gender                 | Male |
      | patient detail primary contact method | SMS  |
    And I click "Save button"
    And I wait 2 seconds
    Then I should EXPECT
      | Window OK | VISIBLE |

  Scenario: upload photo profile
    Given I am ON "New Patient Page"
    And I have clicked "Window OK"
    When I upload
      | Patient image | D:\\Pictures\\Stationery Logo.png |
    And I wait 5 seconds
    And I click "close patient"
    Then I should be ON "PatientsPage"

  Scenario: search for new patient, verify it shown
    Given I am ON "PatientsPage"
    When I enter
      | search | aldira |
    And I enter
      | search | Keys.RETURN |
    And I wait 3 seconds
    Then I should EXPECT
      | firstRow | Aldira1 |

  Scenario: Edit patient info
    Given I am ON "PatientsPage"
    When I check "Aldira1 Raharja" at "grid"
    And I click "edit patient"
    Then I should be ON "New Patient Page"

  Scenario: edit patient details
    Given I am ON "New Patient Page"
    When I enter
      | patient detail first name | Dira |
    And I click "Save button"
    And I wait 2 seconds
    Then I should EXPECT
      | Window OK | VISIBLE |

  Scenario: upload photo profile
    Given I am ON "New Patient Page"
    And I have clicked "Window OK"
    When I click "close patient"
    Then I should be ON "PatientsPage"

  @last
  Scenario: delete patient
    Given I am ON "PatientsPage"
    When I enter
      | search | dira |
    And I enter
      | search | Keys.RETURN |
    And I wait 3 seconds
    And I check "Dira Raharja" at "grid"
    And I click "delete patient"
    And I click "yes button dialog"
