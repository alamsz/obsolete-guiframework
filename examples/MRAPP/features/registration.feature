Feature: Open registration page, try to input and submit 

Scenario: Open registration page 
    Given I am ON "LoginPage" 
    When I click "registerButton" 
    Then I should be ON "RegisterPage" 
    
@last 
Scenario: Open dropdown title 
    Given I am ON "RegisterPage" 
    When I select dropdown 
        | titleDropdown | Lady                                |
        | userCategory  | Guest                               |
        | timezone      | (GMT+07:00) Bangkok, Hanoi, Jakarta |
    And I enter 
        | practiceName | dira                            |
        | firstName    | Aldira                          |
        | lastName     | Raharja                         |
        | email        | aldiraputra.raharja@mitrais.com |
    Then I should EXPECT 
        | titleDropdown | Lady    |