Feature: Create appointment, show drag drop action 

Scenario: Login in 
    Given I have logged in using 
        | username | olivia.charlotte                 |
        | email    | oliviacharlotte.doctor@gmail.com |
        | password | password                         |
        
Scenario: Clicking appointment menu 
    Given I am ON "HomePage" 
    When I click "appointment" 
    Then I should be ON "Appointment Page" 
    
Scenario: Fillin the form 
    Given I am ON "Appointment Page" 
    When I click "new appointment button" 
    And I enter 
        | patientName | seb |
    And I click "firstResultofDropdown" 
    And I select dropdown 
        | urgency      | Urgent           |
        | room         | Room 1           |
        | provider     | Olivia Charlotte |
        | type         | Operation        |
        | type-minutes | 120 Min          |
    And I pick datetime 
        | time | 6/1/2015 10:00 AM |
    And I click "save appointment" 
    Then I should EXPECT 
        | newAppointment | VISIBLE |
        
Scenario: point a cell using doctor and time 
    Given I am ON "Appointment Page" 
    When I double-click on "appointmentGrid" 
        | time    | doctor        |
        | 3:00 PM | Marcus George |
    Then I should EXPECT 
        | providerselected | Marcus George |
        
Scenario: Drag appointment to another position 
    Given I am ON "Appointment Page" 
    And I have clicked "cancelPopup" 
    When I drag "newAppointment" to "targetPlace" 
    And I double-click ON "newAppointment" 
    Then I should EXPECT 
        | providerselected | Zoe Sophia |
        
@last 
Scenario: Delete Appointment 
    Given I am ON "Appointment Page" 
    And I have clicked "cancelPopup" 
    When I delete "newAppointment" 
    And I click "confirmDelete" 
