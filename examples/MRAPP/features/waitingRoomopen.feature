Feature: Create appointment, Change to waiting room, manipulate appointment

  Scenario: Login in
    Given I have logged in using
      | username | olivia.charlotte                 |
      | email    | oliviacharlotte.doctor@gmail.com |
      | password | password                         |

  Scenario: Clicking appointment menu
    Given I am ON "HomePage"
    When I click "appointment"
    Then I should be ON "Appointment Page"

  Scenario: Fillin the form
    Given I am ON "Appointment Page"
    And I have waited 3 seconds
    When I click "new appointment button"
    And I wait 2 seconds
    And I enter
      | patientName | seb |
    And I wait 3 seconds
    And I click "firstResultofDropdown"
    And I select dropdown
      | urgency      | Urgent           |
      | room         | Room 1           |
      | provider     | Olivia Charlotte |
      | type         | Operation        |
      | type-minutes | 120 Min          |
    And I pick datetime
      | time | 10:00 AM |
    And I click "save appointment"
    And I wait 2 seconds
    Then I should EXPECT
      | newAppointment | VISIBLE |

  Scenario: Clicking appointment menu
    Given I am ON "HomePage"
    When I click "waiting room"
    Then I should be ON "Waiting room"

  Scenario: Browse appointment, add to waiting room
    Given I am ON "Waiting room"
    When I click "Appointment browse"
    And I wait 2 seconds
    And I click "First appointment add"
    Then I should EXPECT
      | Waiting room first card | VISIBLE |
    And I click "Waiting room first card"
    Then I should EXPECT
      | First Appointment hover move | VISIBLE |
    And I click "First Appointment hover move"
    Then I should EXPECT
      | With doctor first card | VISIBLE |

  Scenario: Move from with doctor to billing
    Given I am ON "Waiting room"
    When I click "With doctor first card"
    Then I should EXPECT
      | With doctor card hover | VISIBLE |
    And I click "With doctor card hover billing"
    Then I should EXPECT
      | Billing first card | VISIBLE |

  Scenario: Finish the patient
    Given I am ON "Waiting room"
    When I click "Billing first card"
    Then I should EXPECT
      | Billing first card hover | VISIBLE |
    And I click "Billing first card hover bill"

  Scenario: checking status of patient
    Given I am ON "HomePage"
    When I click "appointment"
    And I wait 3 seconds
    Then I should be ON "Appointment Page"
    And I should EXPECT
      | newAppointment | Completed |

  @last
  Scenario: removing patient
    Given I am ON "Appointment Page"
    When I double-click ON "newAppointment"
    And I select dropdown
      | popup option | Delete |
