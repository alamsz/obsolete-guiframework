Feature: Patient page test, expand patient details.

  Scenario: Login in
    Given I have logged in using
      | username | olivia.charlotte                 |
      | email    | oliviacharlotte.doctor@gmail.com |
      | password | password                         |

  Scenario: navigate to patient page
    Given I am ON "HomePage"
    When I click "patients"
    Then I should be ON "PatientsPage"

  Scenario: Expand patient info
    Given I am ON "PatientsPage"
    When I expand "Riley,Max" at "grid"
    Then I should EXPECT
      | patientInfoContent | VISIBLE |
    When I click "familyTab"
    Then I should EXPECT
      | familyContent | VISIBLE |

  Scenario: Sorting test
    Given I am ON "PatientsPage"
    And I have refreshed page
    When I click "name"
    And I wait 3 seconds
    Then I should EXPECT
      | firstRow | Audrey,Holly |

  Scenario: Searching using enter key
    Given I am ON "PatientsPage"
    And I have refreshed page
    And I have waited 5 seconds
    When I enter
      | search | eva |
    And I enter
      | search | Keys.RETURN |
    And I wait 3 seconds
    Then I should EXPECT
      | firstRow | Scarlett,Eva |

  @last
  Scenario: filtering
    Given I am ON "PatientsPage"
    And I have refreshed page
    And I have waited 5 seconds
    When I click "filter"
    And I wait 4 seconds
    And I click "tooracClinicFilter"
    And I wait 3 seconds
    And I expand "Audrey,Holly" at "grid"
    And I wait 3 seconds
    Then I should EXPECT
      | patientInfoContent | The Toorak Clinic |
