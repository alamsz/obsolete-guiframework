Feature: Open login page, test validation is working, and do successful login

  Scenario: Verify that we arrived at Login Page
    Given I am ON "LoginPage"
    Then I should EXPECT
      | LoginPage.username    | visible |      
      | LoginPage.password    | visible |
      | LoginPage.loginButton | visible |
 
  @last
  Scenario: Simulate successful login
    Given I am ON "LoginPage"
    When I enter
      | LoginPage.username | fill username        |      
      | LoginPage.password | fill password        |
    And I click "loginButton"
    Then I should be ON "HomePage"
