//Exported from XMl via http://logback.qos.ch/translator/asGroovy.html

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.FileAppender

import static ch.qos.logback.classic.Level.DEBUG
import static ch.qos.logback.classic.Level.ERROR
import static ch.qos.logback.classic.Level.INFO
import ch.qos.logback.core.status.OnConsoleStatusListener

statusListener(OnConsoleStatusListener)

appender("STDOUT", ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"
  }
}
appender("FILE", FileAppender) {
  file = "D:/mitraisGUITestAutomation.log"
  encoder(PatternLayoutEncoder) {
    pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} [lineNum:%line] - %msg%n"
//    pattern = "%d{HH:mm:ss.SSS} %-5level %logger{36}:%line - %msg%n"
//    pattern = " %msg%n"
  }
}

logger("org.apache", ERROR)
logger("com.mitrais.guitestautomation", DEBUG)
root(DEBUG, ["STDOUT", "FILE"])


