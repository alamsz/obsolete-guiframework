package com.mitrais.guitestautomation.processor;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mitrais.guitestautomation.action.HtmlAction;
import com.mitrais.guitestautomation.action.HtmlActionStatus;
import com.mitrais.guitestautomation.exception.WebActionException;

public class KendoProcessor extends PageProcessor {

    private static final String XPATH_SEARCH_BY_TEXT = "descendant::*[normalize-space(text())='%s']";
    private static final String XPATH_KENDO_ROW = "descendant::tr[contains(@class,'k-master-row')]/";

    private static final Logger LOG = LoggerFactory
            .getLogger(KendoProcessor.class);

    private static KendoProcessor mInstance;

    public static synchronized KendoProcessor getInstance() {
        if (mInstance == null) {
            mInstance = new KendoProcessor();
        }
        return mInstance;
    }

    @Override
    protected HtmlActionStatus customAction(ActionRequest actionRequest)
            throws WebActionException {
        // Element action
        WebElement element = populateElement(actionRequest
                .getElementIdentifier());
        return handleAction(element, actionRequest.getAction(),
                actionRequest.getParameters());
    }

    private HtmlActionStatus handleAction(WebElement element,
            HtmlAction action, List<Object> params) throws WebActionException {
        try {
            switch (action) {
            case KENDO_DROPDOWN:
                handleKendoDropdownAction(element, (String) params.get(0));
                break;
            case KENDO_EXPAND_ROW:
                handleKendoExpandRowAction(element, (String) params.get(0));
                break;
            case KENDO_DATETIME:
                handleKendoDateTimeAction(element, (String) params.get(0));
                break;
            case KENDO_DATE:
                handleKendoDateAction(element, (String) params.get(0));
                break;
            case DELETE:
                handleKendoDeleteAction(element);
                break;
            default:
                break;
            }
            return HtmlActionStatus.GOOD;
        } catch (WebActionException e) {
            LOG.error("Fail executing action", e);
            throw new WebActionException("Fail executing action", e);
        } catch (ParseException e) {
            LOG.error("Parse error", e);
            throw new WebActionException("Fail executing action", e);
        } catch (MalformedURLException e) {
            LOG.error("Malformed URL", e);
            throw new WebActionException("Fail executing action", e);
        }
    }

    @Override
    protected void customScroll(WebElement element) {
        try {
            int movement = 90;
            while (!element.isDisplayed()) {
                WebElement container = populateElement(
                        element,
                        By.xpath("ancestor::*[contains(@class,'mCSB_container')]"));
                String topValue = container.getCssValue("top");
                int startPosition = Integer.valueOf(topValue.substring(0,
                        topValue.indexOf("p")));
                testManager.getJQueryDriver().executeScript(
                        "$('#"
                                + container.getAttribute("id")
                                + "').css('top','"
                                + String.format("%dpx", startPosition
                                        - movement) + "');");
            }
        } catch (MalformedURLException e) {
            LOG.error("Fail to move element ", e);
        }
    }

    private void handleKendoDeleteAction(WebElement element)
            throws WebActionException {
        handleHoverAction(element);
        WebElement deleteButton = populateElement(element,
                By.xpath("../following-sibling::*[1]/a"));
        handleClickAction(deleteButton);
    }

    /**
     * Handle date time pick on KendoUI Framework
     * 
     * @param element
     * @param value
     * @throws ParseException
     * @throws WebActionException
     * @throws MalformedURLException
     */
    private void handleKendoDateTimeAction(WebElement element, String value)
            throws ParseException, WebActionException, MalformedURLException {
        // TODO identify value well..
        if (value.contains("/")) {
            handleKendoDateAction(element, value);
        }

        if (value.contains(":")) {
            WebElement hourButton = populateElement(element,
                    By.xpath("../descendant::*[contains(@class,'k-i-clock')]"));

            handleClickAction(hourButton);

            WebElement hourOptions = populateElement(By.xpath("//ul[@id='"
                    + hourButton.getAttribute("aria-controls") + "']"));
            if (hourOptions.isDisplayed()) {
                String query = "";
                if (value.contains("/")) {
                    query = value.substring(value.indexOf(" ") + 1);
                } else {
                    query = value;
                }
                WebElement containerOptions = populateElement(hourOptions,
                        By.xpath(".."));
                List<WebElement> listOptions = populateElements(hourOptions,
                        By.xpath("li"));
                WebElement hourTarget = performScrolltoElement(
                        containerOptions, listOptions, query);
                if (hourTarget == null) {
                    throw new NoSuchElementException(
                            "Element in options not found");
                } else {
                    handleClickAction(hourTarget);
                }
            } else {
                throw new NoSuchElementException("Options doesnt show");
            }
        }
    }

    private void handleKendoDateAction(WebElement element, String value)
            throws ParseException, WebActionException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        WebElement calendarButton = populateElement(element,
                By.xpath("../descendant::*[contains(@class,'k-i-calendar')]"));

        handleClickAction(calendarButton);

        String popupId = (calendarButton.getAttribute("aria-controls")) != null ? calendarButton
                .getAttribute("aria-controls") : element
                .getAttribute("aria-owns");

        WebElement datepickContainer = populateElement(By.xpath("//*[@id='"
                + popupId + "']"));
        Date date = dateFormat.parse(value);
        Calendar todayCalendar = Calendar.getInstance();
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);

        int yearDiff = targetCalendar.get(Calendar.YEAR)
                - todayCalendar.get(Calendar.YEAR);
        int monthDiff = targetCalendar.get(Calendar.MONTH)
                - todayCalendar.get(Calendar.MONTH);
        monthDiff = monthDiff + (yearDiff * 12);
        WebElement monthControll;
        if (monthDiff > 0) {
            monthControll = populateElement(datepickContainer,
                    By.xpath("descendant::*[contains(@class,'k-nav-next')]"));
        } else {
            monthControll = populateElement(datepickContainer,
                    By.xpath("descendant::*[contains(@class,'k-nav-prev')]"));
        }
        for (int i = 0; i < Math.abs(monthDiff); i++) {
            handleClickAction(monthControll);
        }

        int yearVal = targetCalendar.get(Calendar.YEAR);
        int monthVal = targetCalendar.get(Calendar.MONTH);
        int dayVal = targetCalendar.get(Calendar.DAY_OF_MONTH);

        String dateval = String.format("%d/%d/%d", yearVal, monthVal, dayVal);
        WebElement dateTarget = populateElement(datepickContainer,
                By.xpath("descendant::*[@data-value='" + dateval + "']"));

        handleClickAction(dateTarget);
    }

    /**
     * Handle kendo expand row at grid
     * 
     * @param element
     * @param rowIdentifier
     * @throws WebActionException
     */
    private void handleKendoExpandRowAction(WebElement element,
            String rowIdentifier) throws WebActionException {
        moveToElement(element);
        List<WebElement> rowElements = populateElements(
                element,
                By.xpath(XPATH_KENDO_ROW + "/"
                        + String.format(XPATH_SEARCH_BY_TEXT, rowIdentifier)));
        if (rowElements.size() == 0) {
            throw new NoSuchElementException("row detected = 0");
        }

        for (WebElement row : rowElements) {
            WebElement expandButton = populateElement(
                    row,
                    By.xpath("../preceding-sibling::td[@class='k-hierarchy-cell']"));
            handleClickAction(expandButton);
        }
    }

    /**
     * Handle kendo custom dropdown.
     * 
     * @param element
     * @param selectedValue
     * @throws WebActionException
     * @throws MalformedURLException
     */
    private void handleKendoDropdownAction(WebElement element,
            String selectedValue) throws WebActionException,
            MalformedURLException {
        WebElement dropdownField = populateElement(By
                .xpath("//descendant::input[@id='" + element.getAttribute("id")
                        + "']/.."));
        handleClickAction(dropdownField);
        WebElement optionContainer = populateElement(By.xpath("//ul[@id='"
                + dropdownField.getAttribute("aria-owns") + "']"));
        WebElement target = findElementByText(optionContainer, selectedValue,
                "li");
        if (target == null) {
            WebElement scrollableContainer = populateElement(optionContainer,
                    By.xpath(".."));
            List<WebElement> listOptions = populateElements(optionContainer,
                    By.xpath("li"));
            target = performScrolltoElement(scrollableContainer, listOptions,
                    selectedValue);
        }
        handleClickAction(target);
    }

    /**
     * Performing scroll on Kendo UI Selection.
     * 
     * @param container
     * @param options
     * @param target
     * @return
     * @throws MalformedURLException
     */
    private WebElement performScrolltoElement(WebElement container,
            List<WebElement> options, String target)
            throws MalformedURLException {
        boolean needScroll = false;
        try {
            WebDriverWait wait = new WebDriverWait(testManager.getWebDriver(),
                    testedPage.getElementWaitTimeout());
            wait.until(ExpectedConditions.visibilityOfAllElements(options));
        } catch (TimeoutException e) {
            needScroll = true;
        }

        int movement = 90;
        for (WebElement element : options) {
            if (!element.isDisplayed() && needScroll) {
                String topValue = container.getCssValue("top");
                int startPosition = Integer.valueOf(topValue.substring(0,
                        topValue.indexOf("p")));
                testManager.getJQueryDriver().executeScript(
                        "$('#"
                                + container.getAttribute("id")
                                + "').css('top','"
                                + String.format("%dpx", startPosition
                                        - movement) + "');");
            }
            element = populateElement(element, self);
            if (element.isDisplayed() && element.getText().contains(target)) {
                return element;
            }
        }
        return null;
    }

    /**
     * Find an Element based on Text containing. If multiple element is
     * selected, the first element will be return
     * 
     * @param root
     * @param text
     * @param tagTarget
     * @return
     * @throws NoSuchElementException
     */
    private WebElement findElementByText(WebElement root, String text,
            String tagTarget) throws NoSuchElementException {
        List<WebElement> listofelements = populateElements(root,
                By.xpath(String.format(XPATH_SEARCH_BY_TEXT, text)));
        for (WebElement element : listofelements) {
            if (element.isDisplayed() && element.isEnabled()) {
                if (tagTarget != null && element.getTagName().equals(tagTarget)) {
                    return element;
                } else {
                    return element;
                }
            }
        }
        return null;
    }

}
