package com.mitrais.guitestautomation.processor;

import com.mitrais.guitestautomation.action.HtmlActionStatus;
import com.mitrais.guitestautomation.exception.WebActionException;
import com.mitrais.guitestautomation.page.DomElementExpectation;

public interface ActionProcessor {
    HtmlActionStatus doAction(ActionRequest actionRequest)
            throws WebActionException;

    boolean doExpect(DomElementExpectation expectation);
}
