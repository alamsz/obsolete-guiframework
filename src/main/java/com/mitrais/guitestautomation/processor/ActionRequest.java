package com.mitrais.guitestautomation.processor;

import java.util.ArrayList;
import java.util.List;

import com.mitrais.guitestautomation.action.HtmlAction;

/**
 * Specify an action by creating this instance.
 * @author Aldira_R
 *
 */
public class ActionRequest {
    private HtmlAction action;
    private String elementIdentifier;
    private List<Object> parameters;

    public ActionRequest(HtmlAction action, String elementIdentifier) {
        this.action = action;
        this.elementIdentifier = elementIdentifier;
        parameters = new ArrayList<>();
    }

    public HtmlAction getAction() {
        return action;
    }

    public String getElementIdentifier() {
        return elementIdentifier;
    }

    public List<Object> getParameters() {
        return parameters;
    }
    
    public void addParameters(Object object){
        parameters.add(object);
    }
    
    public Object getParameter(int position){
        return parameters.get(position);
    }
}
