package com.mitrais.guitestautomation.processor;

import org.openqa.selenium.WebElement;

import com.mitrais.guitestautomation.action.HtmlActionStatus;
import com.mitrais.guitestautomation.exception.WebActionException;

/**
 * Class for processing generic HTML.
 * 
 * @author Aldira_R
 *
 */
public class HtmlProcessor extends PageProcessor {

    private static PageProcessor mInstance;

    public static synchronized PageProcessor getInstance() {
        if (mInstance == null) {
            mInstance = new HtmlProcessor();
        }
        return mInstance;
    }

    @Override
    protected HtmlActionStatus customAction(ActionRequest actionRequest)
            throws WebActionException {
        return HtmlActionStatus.NOT_SUPPORTED;
    }

    @Override
    protected void customScroll(WebElement element) {

    }

}
