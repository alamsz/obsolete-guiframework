package com.mitrais.guitestautomation.processor;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.Scrollable;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.mitrais.guitestautomation.WebUIManager;
import com.mitrais.guitestautomation.action.HtmlAction;
import com.mitrais.guitestautomation.action.HtmlActionStatus;
import com.mitrais.guitestautomation.exception.WebActionException;
import com.mitrais.guitestautomation.page.DomElement;
import com.mitrais.guitestautomation.page.DomElementExpectation;
import com.mitrais.guitestautomation.page.HtmlActionConditionEnum;
import com.mitrais.guitestautomation.page.Page;
import com.mitrais.guitestautomation.util.Utils;

/**
 * Class that represent how to do Action in {@link Page}
 * 
 * @author Aldira_R
 *
 */
public abstract class PageProcessor implements ActionProcessor {
    private static final Logger LOG = LoggerFactory
            .getLogger(PageProcessor.class);
    protected static By self = By.xpath(".");

    protected PageProcessor() {
        testManager = WebUIManager.getInstance();
        testManager.getWebDriver().get(testManager.getBaseUrl());
        testManager
                .getWebDriver()
                .manage()
                .timeouts()
                .implicitlyWait(WebUIManager.getPageLoadTimeout(),
                        TimeUnit.SECONDS);
    }

    /**
     * Page to be tested
     */
    protected Page testedPage;

    /**
     * Test Manager that contains timeout, WebDriver, etc.
     */
    protected WebUIManager testManager;

    /**
     * Setup the page and validating page
     * 
     * @param pageIdentifier
     * @throws WebActionException
     */
    public void setup(String pageIdentifier) throws WebActionException {
        testedPage = testManager.getPage(pageIdentifier);
        try {
            new WebDriverWait(testManager.getWebDriver(),
                    WebUIManager.getPageLoadTimeout())
                    .until(new ExpectedCondition<Boolean>() {

                        @Override
                        public Boolean apply(WebDriver input) {
                            // checking state by js
                            if (!((JavascriptExecutor) input).executeScript(
                                    "return document.readyState").equals(
                                    "complete")) {
                                return false;
                            }

                            // checking title of page
                            boolean doTitle = (testedPage.getTitle() != null)
                                    && !testedPage.getTitle().trim().isEmpty();

                            boolean urlCorrect = input.getCurrentUrl()
                                    .contains(testedPage.getUrlExtension());
                            boolean urlAndTitleCorrect = doTitle ? (urlCorrect && input
                                    .getTitle().equals(testedPage.getTitle()))
                                    : urlCorrect;
                            boolean keyComponentExisted = true;

                            if (testedPage.getKeyDomElementName() != null) {
                                WebElement keyDomComponent = populateElement(testedPage
                                        .getKeyDomElementName());

                                if (keyDomComponent == null) {
                                    LOG.info("There is no such domElement, ["
                                            + testedPage.getKeyDomElementName()
                                            + "] in your configuration.");

                                    return false;
                                }

                                if (!keyDomComponent.isEnabled()
                                        || !keyDomComponent.isDisplayed()) {
                                    keyComponentExisted = false;
                                }
                            }
                            return urlAndTitleCorrect && keyComponentExisted;
                        }

                    });
        } catch (Exception e) {
            LOG.error("Exception on setup page", e);
            throw new WebActionException("Exception on setup page", e);
        }
    }

    @Override
    public boolean doExpect(DomElementExpectation expectation) {
        String expectedValue = (expectation.getValue() == null) ? ""
                : expectation.getValue();
        String notString = expectation.isNegative() ? " not " : " ";

        LOG.info("Checking expectation: " + expectation.getComponentName()
                + notString + expectation.getCondition() + ' ' + expectedValue);

        boolean expectationMet = true;

        String condition = expectation.getCondition();

        HtmlActionConditionEnum htmlAction = HtmlActionConditionEnum
                .findValue(condition);

        switch (htmlAction) {
        case EXISTS:
            expectationMet = handleExistsAction(expectation);
            break;
        case VISIBLE:
            expectationMet = handleVisibleAction(expectation);
            break;
        case INVISIBLE:
            expectationMet = handleInvisibleAction(expectation);
            break;
        default:
            DomElement domElement = prepareElement(expectation);
            if (domElement == null) {
                expectationMet = false;
            } else {
                expectationMet = valueCheck(htmlAction, expectation,
                        domElement, expectationMet);
            }
            break;
        }

        return expectationMet;
    }

    private DomElement prepareElement(DomElementExpectation expectation) {
        String componentName = expectation.getComponentName();
        DomElement domElement = testedPage.getDomElement(componentName);
        return domElement;
    }

    private boolean handleExistsAction(DomElementExpectation expectation) {
        boolean result = false;
        DomElement domElement = prepareElement(expectation);
        if (domElement != null && domElement.isExisted()) {
            result = true;
        }

        return (expectation.isNegative()) ? !result : result;
    }

    private boolean handleVisibleAction(DomElementExpectation expectation) {
        boolean result = false;
        DomElement domElement = prepareElement(expectation);
        try {
            WebElement webElement = populateElement(domElement);
            result = (webElement != null) && webElement.isDisplayed()
                    && webElement.isEnabled();
            return (expectation.isNegative()) ? !result : result;
        } catch (WebDriverException e) {
            LOG.error("expected visible", e);
        }
        return result;
    }

    private boolean handleInvisibleAction(DomElementExpectation expectation) {
        boolean result = false;
        DomElement domElement = prepareElement(expectation);
        WebElement webElement = domElement.getDomElement();
        result = (webElement == null)
                || !(webElement.isDisplayed() && webElement.isEnabled());
        return (expectation.isNegative()) ? !result : result;
    }

    private boolean valueCheck(HtmlActionConditionEnum htmlAction,
            DomElementExpectation expectation, DomElement domElement,
            boolean expectationMet) {
        String htmlValue = getDomElementHtmlValue(domElement);

        htmlValue = htmlValue.replace("&nbsp;", " ").trim();

        boolean result = false;

        if (htmlAction == HtmlActionConditionEnum.EQUALS) {
            result = handleEqualsAction(expectation, htmlValue);
        } else if (htmlAction == HtmlActionConditionEnum.NOT_EQUALS) {
            result = handleNotEqualsAction(expectation, htmlValue);
        } else {
            double expectDoubleValue = Double.parseDouble(expectation
                    .getValue());
            String actualValue = htmlValue.replaceAll("[^0-9\\.]", "");
            double actualDoubleValue = Double.parseDouble(actualValue);

            switch (htmlAction) {
            case GREATER_EQUAL_THAN:
                result = handleGreaterEqualThanAction(expectation,
                        expectDoubleValue, actualDoubleValue);
                break;
            case GREATER_THAN:
                result = handleGreaterThanAction(expectation,
                        expectDoubleValue, actualDoubleValue);
                break;
            case LESS_EQUAL_THAN:
                result = handleLessThanEqualAction(expectation,
                        expectDoubleValue, actualDoubleValue);
                break;
            case LESS_THAN:
                result = handleLessThanAction(expectation, expectDoubleValue,
                        actualDoubleValue);
                break;
            default:
                break;
            }
        }
        return result;
    }

    protected String getDomElementHtmlValue(DomElement domElement) {
        String value = "";

        if (domElement.getJquery() != null) {
            LOG.info("find html value jquery: " + "return "
                    + domElement.getJqueryGetHtml());
            try {
                value = (String) testManager.getJQueryDriver().executeScript(
                        "return " + domElement.getJqueryGetHtml());
            } catch (MalformedURLException e) {
                LOG.error("JQuery exception", e);
            }
        } else {
            value = populateElement(domElement).getAttribute("innerHTML");
        }

        return value;
    }

    private boolean handleEqualsAction(DomElementExpectation expectation,
            String htmlValue) {
        boolean result = true;
        // if string, we only care if the expected value was contained.
        if (Utils.isANumber(expectation.getValue())) {
            double expectValue = Double.parseDouble(expectation.getValue());
            double actualValue = Double.parseDouble(htmlValue.replaceAll(
                    "[^0-9\\.]", ""));

            // if they differ by more than a small amount...
            if (Math.abs(expectValue - actualValue) > 0.00001d) {
                LOG.error("Expectation failed (expected equals): expects "
                        + expectation.getValue() + "; actual = [" + htmlValue
                        + ']');
                result = false;
            }
        } else {
            if (expectation.isCustomAction()) {
                if (!htmlValue.contains(expectation.getValue().trim())) {
                    WebElement element = populateElement(prepareElement(expectation));
                    try {
                        WebElement kendoElement = populateElement(element,
                                By.xpath("preceding-sibling::*[1]"));
                        if (!kendoElement.getText().contains(
                                expectation.getValue().trim())) {
                            result = false;
                            LOG.error("Expectation failed (expected equals): expects "
                                    + expectation.getValue()
                                    + "; actual = ["
                                    + htmlValue + ']');
                        }
                    } catch (NoSuchElementException elementException) {
                        result = false;
                        LOG.error("Expectation failed (expected equals): expects "
                                + expectation.getValue()
                                + "; actual = ["
                                + htmlValue + ']');
                    }
                }
            }
        }
        return result;
    }

    private boolean handleNotEqualsAction(DomElementExpectation expectation,
            String htmlValue) {
        boolean result = false;
        if (!expectation.getValue().equalsIgnoreCase(htmlValue.trim())) {
            result = true;
        }

        return result;
    }

    private boolean handleGreaterEqualThanAction(
            DomElementExpectation expectation, double expectDoubleValue,
            double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue > actualDoubleValue) {
            result = false;
        }

        return result;
    }

    private boolean handleLessThanEqualAction(DomElementExpectation expect,
            double expectDoubleValue, double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue < actualDoubleValue) {
            result = false;
        }

        return result;
    }

    private boolean handleLessThanAction(DomElementExpectation expect,
            double expectDoubleValue, double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue <= actualDoubleValue) {
            result = false;
        }

        return result;
    }

    private boolean handleGreaterThanAction(DomElementExpectation expectation,
            double expectDoubleValue, double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue >= actualDoubleValue) {
            result = false;
        }

        return result;
    }

    @Override
    public HtmlActionStatus doAction(ActionRequest actionRequest)
            throws WebActionException {
        // Non element action
        HtmlAction action = actionRequest.getAction();
        if (action == HtmlAction.WAIT) {
            handleWaitAction((int) actionRequest.getParameter(0));
            return HtmlActionStatus.GOOD;
        } else if (action == HtmlAction.REFRESH) {
            handleRefreshAction();
            return HtmlActionStatus.GOOD;
        }

        // Element action
        WebElement element = populateElement(actionRequest
                .getElementIdentifier());
        try {
            HtmlActionStatus result = handleAction(element, action,
                    actionRequest.getParameters());
            if (result == HtmlActionStatus.NOT_SUPPORTED) {
                return customAction(actionRequest);
            }
            return result;
        } catch (WebActionException e) {
            LOG.error("Exception on executing action", e);
            throw new WebActionException("Exception on executing action", e);
        }
    }

    protected abstract HtmlActionStatus customAction(ActionRequest actionRequest)
            throws WebActionException;

    /**
     * Generic do Action to execute action at element
     * 
     * @param element
     * @param action
     * @param params
     * @return
     * @throws WebActionException
     */
    private HtmlActionStatus handleAction(WebElement element,
            HtmlAction action, List<Object> params) throws WebActionException {
        try {
            switch (action) {
            case CLICK:
                handleClickAction(element);
                break;
            case HOVER:
                handleHoverAction(element);
                break;
            case ENTER:
                handleEnterAction(element, (String) params.get(0));
                break;
            case DOUBLE_CLICK:
                handleDoubleClickAction(element);
                break;
            case DRAG:
                handleDragAction(element, (String) params.get(0));
                break;
            default:
                LOG.info("Can't handle action");
                return HtmlActionStatus.NOT_SUPPORTED;
            }
            return HtmlActionStatus.GOOD;
        } catch (WebActionException e) {
            LOG.error("Exception on executing action", e);
            throw new WebActionException("Exception on executing action", e);
        }
    }

    protected void handleDragAction(WebElement element, final String target) {
        moveToElement(element);
        Wait<WebElement> loader = new FluentWait<WebElement>(element)
                .ignoring(StaleElementReferenceException.class)
                .withTimeout((long) testedPage.getElementWaitTimeout(),
                        TimeUnit.SECONDS)
                .pollingEvery(testedPage.getElementPoolingTimeout(),
                        TimeUnit.MILLISECONDS);
        loader.until(new Function<WebElement, Boolean>() {
            @Override
            public Boolean apply(WebElement originPosition) {
                Actions actions = new Actions(testManager.getWebDriver());
                WebElement targetPosition = populateElement(target);
                actions.dragAndDrop(originPosition, targetPosition).perform();
                return true;
            }
        });
    }

    /**
     * Handle double click action
     * 
     * @param element
     */
    protected void handleDoubleClickAction(WebElement element) {
        moveToElement(element);
        Wait<WebElement> loader = new FluentWait<WebElement>(element)
                .withTimeout((long) testedPage.getElementWaitTimeout(),
                        TimeUnit.SECONDS)
                .pollingEvery(testedPage.getElementPoolingTimeout(),
                        TimeUnit.MILLISECONDS)
                .ignoring(StaleElementReferenceException.class);
        loader.until(new Function<WebElement, Boolean>() {

            @Override
            public Boolean apply(WebElement input) {
                Actions webActions = new Actions(testManager.getWebDriver());
                webActions.doubleClick(populateElement(input, self)).build()
                        .perform();
                return true;
            }
        });
    }

    /**
     * Handle enter statement
     * 
     * @param element
     * @param value
     * @throws WebActionException
     */
    private void handleEnterAction(WebElement element, String value)
            throws WebActionException {
        moveToElement(element);
        if (value.startsWith("Keys.")) {
            String keys = value.replace("Keys.", "");
            element.sendKeys(Keys.valueOf(keys));
        } else {
            handleClickAction(element);
            element.clear();
            element.sendKeys(value);
        }
    }

    /**
     * Wait action
     * 
     * @param duration
     */
    protected void handleWaitAction(int duration) {
        try {
            Thread.sleep(TimeUnit.MILLISECONDS.convert(duration,
                    TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            LOG.error("Wait is interrupted", e);
        }
    }

    /**
     * Refresh the page.
     */
    protected void handleRefreshAction() {
        testManager.getWebDriver().navigate().refresh();
    }

    /**
     * Load element from Page.domElements
     * 
     * @param identifier
     * @return
     */
    protected WebElement populateElement(String identifier) {
        DomElement element = testedPage.getDomElement(identifier);
        return populateElement(element);
    }

    /**
     * Populate element directly from domElement.
     * 
     * @param element
     * @return
     */
    protected WebElement populateElement(DomElement element) {
        if (element.getId() != null) {
            return populateElement(By.id(element.getId()));
        } else if (element.getName() != null) {
            return populateElement(By.name(element.getName()));
        } else if (element.getLinkText() != null) {
            return populateElement(By.linkText(element.getLinkText()));
        } else if (element.getClassName() != null) {
            return populateElement(By.className(element.getClassName()));
        } else if (element.getXpath() != null) {
            return populateElement(By.xpath(element.getXpath()));
        } else if (element.getCssSelector() != null) {
            return populateElement(By.cssSelector(element.getCssSelector()));
        } else if (element.getPartialLinkText() != null) {
            return populateElement(By.partialLinkText(element
                    .getPartialLinkText()));
        } else if (element.getTagName() != null) {
            return populateElement(By.tagName(element.getTagName()));
        } else {
            return null;
        }
    }

    /**
     * Terminate test. Close browser and kill the process
     */
    public void terminateProcessor() {
        testManager.terminateWebDriver();
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException}
     * 
     * @param by
     * @return
     */
    protected WebElement populateElement(By by) {
        return populateElement(null, by);
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException} with
     * root element
     * 
     * @param element
     *            element root of element
     * @param by
     *            query of element based on root
     * @return
     */
    protected WebElement populateElement(WebElement element, By by) {
        return populateClickableElement(element, by, false);
    }

    protected WebElement populateClickableElement(final WebElement webElement,
            By by, final boolean clickable) {
        Wait<By> loader = new FluentWait<By>(by)
                .ignoring(WebDriverException.class)
                .withTimeout((long) testedPage.getElementWaitTimeout(),
                        TimeUnit.SECONDS)
                .pollingEvery(testedPage.getElementPoolingTimeout(),
                        TimeUnit.MILLISECONDS);
        return loader.until(new Function<By, WebElement>() {

            @Override
            public WebElement apply(By input) {
                WebElement result = (webElement != null) ? webElement
                        .findElement(input) : testManager.getWebDriver()
                        .findElement(input);

                if (!clickable) {
                    return result;
                }
                return new WebDriverWait(testManager.getWebDriver(),
                        (long) testedPage.getElementWaitTimeout())
                        .ignoring(NoSuchElementException.class)
                        .ignoring(StaleElementReferenceException.class)
                        .until(ExpectedConditions.elementToBeClickable(result));
            }

        });
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException}
     * 
     * @param by
     * @return
     */
    protected List<WebElement> populateElements(By by) {
        return populateElements(null, by);
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException} with
     * root element
     * 
     * @param element
     *            root of element
     * @param by
     *            query of element based on root
     * @return
     */
    protected List<WebElement> populateElements(final WebElement element, By by) {
        Wait<By> loader = new FluentWait<By>(by)
                .withTimeout((long) testedPage.getElementWaitTimeout(),
                        TimeUnit.SECONDS)
                .pollingEvery(testedPage.getElementPoolingTimeout(),
                        TimeUnit.MILLISECONDS)
                .ignoring(WebDriverException.class);
        return loader.until(new Function<By, List<WebElement>>() {

            @Override
            public List<WebElement> apply(By input) {
                return (element != null) ? element.findElements(input)
                        : testManager.getWebDriver().findElements(input);
            }

        });
    }

    /**
     * Handling click action
     * 
     * @param element
     * @throws WebActionException
     */
    protected void handleClickAction(WebElement element)
            throws WebActionException {
        try {
            moveToElement(element);
            Wait<WebElement> loader = new FluentWait<WebElement>(
                    populateClickableElement(element, self, true))
                    .withTimeout((long) testedPage.getElementWaitTimeout(),
                            TimeUnit.SECONDS)
                    .pollingEvery(testedPage.getElementPoolingTimeout(),
                            TimeUnit.MILLISECONDS)
                    .ignoring(WebDriverException.class);
            loader.until(new Function<WebElement, Boolean>() {

                @Override
                public Boolean apply(WebElement input) {
                    try {
                        boolean animated = (boolean) testManager
                                .getJQueryDriver()
                                .executeScript(
                                        "return $(arguments[0]).is(':animated') && $(':animated').length == 0",
                                        input);
                        if (animated) {
                            System.out.println("Animation detected");
                            return false;
                        }
                    } catch (MalformedURLException e) {
                        LOG.error("Malform jquery driver", e);
                    }
                    input.click();
                    return true;
                }
            });
        } catch (WebDriverException wde) {
            LOG.error("Failed to click", wde);
            throw new WebActionException("Error executing click event.", wde);
        }
    }

    /**
     * Moving screen to correspond Element. Implement {@link Scrollable} for
     * custom element scroll
     * 
     * @param element
     */
    protected void moveToElement(WebElement element) {
        try {
            if (!element.isDisplayed()) {
                JavascriptExecutor executor = testManager.getJQueryDriver();
                executor.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
                executor.executeScript("arguments[0].scrollIntoView();",
                        element);
                if (!element.isDisplayed()) {
                    customScroll(element);
                }
            }
        } catch (MalformedURLException e) {
            LOG.error("Fail to move element ", e);
        }
    }

    /**
     * Do custom scroll according to framework, with element reference
     * 
     * @param element
     */
    protected abstract void customScroll(WebElement element);

    /**
     * Hover action
     * 
     * @param element
     */
    protected void handleHoverAction(WebElement element) {
        moveToElement(element);
        Actions actions = new Actions(testManager.getWebDriver());
        actions.moveToElement(element).perform();
    }
}
