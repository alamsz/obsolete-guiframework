package com.mitrais.guitestautomation.action;

public enum HtmlAction {
    CLICK("click"), HOVER("hover"), WAIT("wait"), REFRESH("refresh"), ENTER(
            "enter"), SELECT("select"), FORWARD("forward"), BACKWARD("backward"), KENDO_DROPDOWN(
            "dropdown"), KENDO_EXPAND_ROW("expand"), DOUBLE_CLICK(
            "double-click"), DRAG("drag"), KENDO_DATETIME("datetime"), CHECK(
            "check"), KENDO_DATE("date"), UPLOAD("upload"), DELETE("delete");

    private String actionName;

    HtmlAction(String actionName) {
        this.actionName = actionName;
    }

    public String getActionName() {
        return actionName;
    }

    public String toString() {
        return getActionName().toUpperCase();
    }
}
