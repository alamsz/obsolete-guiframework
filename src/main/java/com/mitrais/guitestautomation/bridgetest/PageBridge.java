package com.mitrais.guitestautomation.bridgetest;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.mitrais.guitestautomation.WebUIManager;
import com.mitrais.guitestautomation.action.HtmlAction;
import com.mitrais.guitestautomation.action.HtmlActionStatus;
import com.mitrais.guitestautomation.bridgetest.model.SchedulerCell;
import com.mitrais.guitestautomation.exception.InitializationException;
import com.mitrais.guitestautomation.exception.WebActionException;
import com.mitrais.guitestautomation.page.Action;
import com.mitrais.guitestautomation.page.DomElement;
import com.mitrais.guitestautomation.page.DomElementExpectation;
import com.mitrais.guitestautomation.page.HtmlActionConditionEnum;
import com.mitrais.guitestautomation.page.Page;
import com.mitrais.guitestautomation.util.Browsers;
import com.mitrais.guitestautomation.util.Constants;
import com.mitrais.guitestautomation.util.ScreenshotMode;
import com.mitrais.guitestautomation.util.Utils;

public class PageBridge extends Page {

    private static final Logger LOG = LoggerFactory.getLogger(PageBridge.class);

    private static final String XPATH_SEARCH_BY_TEXT = "descendant::*[normalize-space(text())='%s']";
    private static final String XPATH_APPOINTMENT_SCHEDULER_TIME = "//*[@id='%s']/descendant::*[@class='k-scheduler-times']/descendant::*[@class='k-slot-cell']";
    private static final String XPATH_APPOINTMENT_DOCTORS = "//*[@id='%s']/descendant::*[@class='k-scheduler-header-wrap']/descendant::*[@class='k-slot-cell']";
    private static final String XPATH_APPOINTMENT_CELL = "//*[@id='%s']/descendant::tr[@class = 'k-middle-row'][%d]/td[@class='k-today'][%d]";
    private static final String XPATH_KENDO_ROW = "descendant::tr[contains(@class,'k-master-row')]/";
    private static By self = By.xpath(".");

    private ScreenshotMode screenshotMode;

    // ------------------------------START Evaluation
    // method----------------------------------//

    @Override
    public boolean checkExpectation(DomElementExpectation expectation) {

        try {
            Thread.sleep(TimeUnit.MILLISECONDS.convert((long) actionPace,
                    TimeUnit.SECONDS));
        } catch (InterruptedException e1) {
            LOG.error("Interrupted", e1);
            return false;
        }

        String expectedValue = (expectation.getValue() == null) ? ""
                : expectation.getValue();
        String notString = expectation.isNegative() ? " not " : " ";

        LOG.info("Checking expectation: " + expectation.getComponentName()
                + notString + expectation.getCondition() + ' ' + expectedValue);

        boolean expectationMet = true;

        String condition = expectation.getCondition();

        HtmlActionConditionEnum htmlAction = HtmlActionConditionEnum
                .findValue(condition);

        switch (htmlAction) {
        case EXISTS:
            expectationMet = handleExistsAction(expectation);
            break;
        case VISIBLE:
            expectationMet = handleVisibleAction(expectation);
            break;
        case INVISIBLE:
            expectationMet = handleInvisibleAction(expectation);
            break;
        default:
            DomElement domElement = prepareElement(expectation);
            if (domElement == null) {
                expectationMet = false;
            } else {
                expectationMet = valueCheck(htmlAction, expectation,
                        domElement, expectationMet);
            }
            break;
        }

        return expectationMet;
    }

    private DomElement prepareElement(DomElementExpectation expectation) {
        String componentName = expectation.getComponentName();
        DomElement domElement = getDomElement(componentName);
        try {
            populateDomElement(domElement);
        } catch (NoSuchElementException noSuchElementException) {
            domElement = null;
            LOG.error(noSuchElementException.getMessage());
        } catch (WebActionException e) {
            LOG.error(e.getMessage());
        }
        return domElement;
    }

    private boolean handleInvisibleAction(DomElementExpectation expectation) {
        boolean result = false;
        DomElement domElement = prepareElement(expectation);
        WebElement webElement = domElement.getDomElement();
        result = (webElement == null)
                || !(webElement.isDisplayed() && webElement.isEnabled());
        return (expectation.isNegative()) ? !result : result;
    }

    private boolean handleVisibleAction(DomElementExpectation expectation) {
        boolean result = false;
        DomElement domElement = prepareElement(expectation);
        WebElement webElement = domElement.getDomElement();

        result = (webElement != null) && webElement.isDisplayed()
                && webElement.isEnabled();
        return (expectation.isNegative()) ? !result : result;
    }

    private boolean handleExistsAction(DomElementExpectation expectation) {
        boolean result = false;
        DomElement domElement = prepareElement(expectation);
        if (domElement != null && domElement.isExisted()) {
            result = true;
        }

        return (expectation.isNegative()) ? !result : result;
    }

    private boolean valueCheck(HtmlActionConditionEnum htmlAction,
            DomElementExpectation expectation, DomElement domElement,
            boolean expectationMet) {
        String htmlValue = getDomElementHtmlValue(domElement);

        htmlValue = htmlValue.replace("&nbsp;", " ").trim();

        boolean result = false;

        if (htmlAction == HtmlActionConditionEnum.EQUALS) {
            result = handleEqualsAction(expectation, htmlValue);
        } else if (htmlAction == HtmlActionConditionEnum.NOT_EQUALS) {
            result = handleNotEqualsAction(expectation, htmlValue);
        } else {
            double expectDoubleValue = Double.parseDouble(expectation
                    .getValue());
            String actualValue = htmlValue.replaceAll("[^0-9\\.]", "");
            double actualDoubleValue = Double.parseDouble(actualValue);

            switch (htmlAction) {
            case GREATER_EQUAL_THAN:
                result = handleGreaterEqualThanAction(expectation,
                        expectDoubleValue, actualDoubleValue);
                break;
            case GREATER_THAN:
                result = handleGreaterThanAction(expectation,
                        expectDoubleValue, actualDoubleValue);
                break;
            case LESS_EQUAL_THAN:
                result = handleLessThanEqualAction(expectation,
                        expectDoubleValue, actualDoubleValue);
                break;
            case LESS_THAN:
                result = handleLessThanAction(expectation, expectDoubleValue,
                        actualDoubleValue);
                break;
            default:
                break;
            }
        }
        return result;
    }

    private boolean handleLessThanAction(DomElementExpectation expect,
            double expectDoubleValue, double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue <= actualDoubleValue) {
            result = false;
        }

        return result;
    }

    private boolean handleLessThanEqualAction(DomElementExpectation expect,
            double expectDoubleValue, double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue < actualDoubleValue) {
            result = false;
        }

        return result;
    }

    private boolean handleGreaterEqualThanAction(
            DomElementExpectation expectation, double expectDoubleValue,
            double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue > actualDoubleValue) {
            result = false;
        }

        return result;
    }

    private boolean handleGreaterThanAction(DomElementExpectation expectation,
            double expectDoubleValue, double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue >= actualDoubleValue) {
            result = false;
        }

        return result;
    }

    private boolean handleNotEqualsAction(DomElementExpectation expectation,
            String htmlValue) {
        boolean result = false;
        if (!expectation.getValue().equalsIgnoreCase(htmlValue.trim())) {
            result = true;
        }

        return result;
    }

    private boolean handleEqualsAction(DomElementExpectation expectation,
            String htmlValue) {
        boolean result = true;
        // if string, we only care if the expected value was contained.
        if (Utils.isANumber(expectation.getValue())) {
            double expectValue = Double.parseDouble(expectation.getValue());
            double actualValue = Double.parseDouble(htmlValue.replaceAll(
                    "[^0-9\\.]", ""));

            // if they differ by more than a small amount...
            if (Math.abs(expectValue - actualValue) > 0.00001d) {
                LOG.error("Expectation failed (expected equals): expects "
                        + expectation.getValue() + "; actual = [" + htmlValue
                        + ']');
                result = false;
            }
        } else {
            if (expectation.isCustomAction()) {
                if (!htmlValue.contains(expectation.getValue().trim())) {
                    WebElement element = populateElement(prepareElement(expectation));
                    try {
                        WebElement kendoElement = populateElement(element,
                                By.xpath("preceding-sibling::*[1]"));
                        if (!kendoElement.getText().contains(
                                expectation.getValue().trim())) {
                            result = false;
                            LOG.error("Expectation failed (expected equals): expects "
                                    + expectation.getValue()
                                    + "; actual = ["
                                    + htmlValue + ']');
                        }
                    } catch (NoSuchElementException elementException) {
                        result = false;
                        LOG.error("Expectation failed (expected equals): expects "
                                + expectation.getValue()
                                + "; actual = ["
                                + htmlValue + ']');
                    }
                }
            }
        }
        return result;
    }

    // ------------------------------END Evaluation
    // method----------------------------------//

    // -------------------------------START Action
    // Method-----------------------------------//
    @Override
    protected HtmlActionStatus performDomAction(Object element,
            HtmlAction webAction, String actionValue) throws WebActionException {

        try {
            Thread.sleep(TimeUnit.MILLISECONDS.convert((long) actionPace,
                    TimeUnit.SECONDS));
        } catch (InterruptedException e1) {
            throw new WebActionException("Interupted", e1);
        }

        boolean fail = false;
        acceptAlertifPresent();
        try {
            switch (webAction) {
            case ENTER:
                LOG.info("Entering " + actionValue + " to "
                        + ((DomElement) element).getIdentifier());
                handleEnterAction((DomElement) element, actionValue);
                break;
            case CLICK:
                LOG.info("Clicking to "
                        + ((DomElement) element).getIdentifier());
                DomElement domElement = (DomElement) element;
                handleClickAction(domElement);
                break;
            case HOVER:
                LOG.info("Hovering to "
                        + ((DomElement) element).getIdentifier());
                handleHoverAction((DomElement) element);
                break;
            case SELECT:
                LOG.info("Selecting " + actionValue + " at "
                        + ((DomElement) element).getIdentifier());
                performSelectAction((DomElement) element, actionValue);
                break;
            case WAIT:
                try {
                    Thread.sleep(TimeUnit.MILLISECONDS.convert(
                            Long.valueOf(actionValue), TimeUnit.SECONDS));
                } catch (InterruptedException e) {
                    throw new WebActionException("Interupted", e);
                }
                break;
            case REFRESH:
                webDriver.navigate().refresh();
                break;
            case KENDO_DROPDOWN:
                LOG.info("Selecting " + actionValue + " at "
                        + ((DomElement) element).getIdentifier());
                performKendoSelect((DomElement) element, actionValue);
                break;
            case KENDO_EXPAND_ROW:
                LOG.info("Expanding " + actionValue + " at "
                        + ((DomElement) element).getIdentifier());
                handleKendoExpandRow((DomElement) element, actionValue);
                break;
            case DOUBLE_CLICK:
                if (actionValue == null) {
                    LOG.info("Double click at "
                            + ((DomElement) element).getIdentifier());
                    handleDoubleClick((DomElement) element);
                } else {
                    performDoubleClick((SchedulerCell) element, actionValue);
                }
                break;
            case DRAG:
                LOG.info("Dragging " + ((DomElement) element).getIdentifier()
                        + " to " + actionValue);
                handleDragEvent((DomElement) element, actionValue);
                break;
            case KENDO_DATETIME:
                LOG.info("Pick Datetime " + actionValue + " at "
                        + ((DomElement) element).getIdentifier());
                handleDateTimePick((DomElement) element, actionValue);
                break;
            case DELETE:
                LOG.info("Deleting at "
                        + ((DomElement) element).getIdentifier());
                handleDelete((DomElement) element);
                break;
            case KENDO_DATE:
                LOG.info("Pick Date " + actionValue + " at "
                        + ((DomElement) element).getIdentifier());
                handleKendoDatePick((DomElement) element, actionValue);
                break;
            case UPLOAD:
                LOG.info("Upload " + actionValue + " at "
                        + ((DomElement) element).getIdentifier());
                handleUpload((DomElement) element, actionValue);
                break;
            case CHECK:
                LOG.info("Checked " + actionValue + " at "
                        + ((DomElement) element).getIdentifier());
                handleCheckRow((DomElement) element, actionValue);
                break;
            default:
                // do nothing
                break;
            }
            return HtmlActionStatus.GOOD;
        } catch (WebDriverException wde) {
            LOG.error(wde.getMessage());
            fail = true;
            WebActionException wae = new WebActionException(
                    "Element is not actionable", wde);

            throw wae;
        } finally {
            switch (getScreenshotMode()) {
            case ALL:
                takeScreenshot();
                break;
            case ERROR_ONLY:
                if (fail) {
                    takeScreenshot();
                }
                break;
            }
        }
    }

    @Override
    protected void handleClickAction(DomElement domElement)
            throws WebActionException {
        // This will perform click once. If first click fail, then the second
        // click will be called
        if (performClick(domElement.getDomElement())
                || performClick(populateElement(domElement.getDomElement(),
                        self))) {
            return;
        }
        throw new WebActionException("failed to click at "
                + domElement.getIdentifier());
    }

    /**
     * Handle Kendo UI date pick
     * 
     * @param domElement
     * @param value
     * @throws WebActionException
     */
    protected void handleKendoDatePick(DomElement domElement, String value)
            throws WebActionException {
        try {
            performKendoDatePick(domElement.getDomElement(), value);
        } catch (WebActionException e) {
            throw new WebActionException("Failed to pick " + value + " at "
                    + domElement.getIdentifier(), e);
        }
    }

    /**
     * Handling request for drag action
     * 
     * @param domElement
     * @param targetPosition
     * @throws WebActionException
     */
    private void handleDragEvent(DomElement domElement, String targetPosition)
            throws WebActionException {
        try {
            WebElement originElement = populateElement(domElement);

            DomElement destinationElement = getDomElement(targetPosition);
            WebElement destElement = populateElement(destinationElement);

            if (!isDisplayed(originElement)) {
                jsDriver.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
                jsDriver.executeScript("arguments[0].scrollIntoView();",
                        originElement);
            }

            if (!isDisplayed(destElement)) {
                jsDriver.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
                jsDriver.executeScript("arguments[0].scrollIntoView();",
                        destElement);
            }

            if (originElement.isDisplayed() && destElement.isDisplayed()) {
                doDrag(originElement, destElement);
            } else {
                throw new WebActionException(
                        "Failed to drag element, because Element not displayed");
            }
        } catch (WebDriverException | WebActionException e) {
            throw new WebActionException("Failed to drag "
                    + domElement.getIdentifier() + " to " + targetPosition, e);
        }
    }

    private boolean isDisplayed(WebElement element) {
        Wait<WebElement> wait = new FluentWait<WebElement>(element)
                .ignoring(StaleElementReferenceException.class)
                .withTimeout((long) elementWaitTimeout, TimeUnit.SECONDS)
                .pollingEvery(elementPoolingTimeout, TimeUnit.MILLISECONDS);
        return wait.until(new Function<WebElement, Boolean>() {

            @Override
            public Boolean apply(WebElement input) {
                return populateElement(input, self).isDisplayed();
            }

        });
    }

    private void doDrag(final WebElement origin, final WebElement destination) {
        Actions actions = new Actions(webDriver);
        Wait<Actions> loader = new FluentWait<Actions>(actions)
                .ignoring(StaleElementReferenceException.class)
                .withTimeout((long) elementWaitTimeout, TimeUnit.SECONDS)
                .pollingEvery(elementPoolingTimeout, TimeUnit.MILLISECONDS);
        loader.until(new Function<Actions, Boolean>() {
            @Override
            public Boolean apply(Actions actions) {
                actions.dragAndDrop(populateElement(origin, self),
                        populateElement(destination, self)).perform();
                return true;
            }
        });
    }

    /**
     * Handle double click action on element
     * 
     * @param domElement
     * @throws WebActionException
     */
    protected void handleDoubleClick(DomElement domElement)
            throws WebActionException {
        try {
            performDoubleClick(populateElement(domElement));
        } catch (WebActionException e) {
            throw new WebActionException("Failed to double click "
                    + domElement.getIdentifier(), e);
        }
    }

    @Override
    protected void handleEnterAction(DomElement domElement, String actionValue)
            throws WebActionException {
        try {
            if (domElement.getDomElement().isDisplayed()) {
                performEnterAction(domElement.getDomElement(), actionValue);
            } else {
                jsDriver.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
                jsDriver.executeScript("arguments[0].scrollIntoView();",
                        domElement.getDomElement());
                WebElement enterElement = populateElement(domElement);
                performEnterAction(enterElement, actionValue);
            }
        } catch (Exception e) {
            throw new WebActionException("Failed to enter " + actionValue
                    + " at " + domElement.getIdentifier(), e);
        }
    }

    private void takeScreenshot() {
        File screenshot = ((TakesScreenshot) webDriver)
                .getScreenshotAs(OutputType.FILE);
        File screenshotFolder = new File(
                System.getProperty(WebUIManager.SYSTEM_SCREENSHOT_PATH));
        if (!screenshotFolder.exists()) {
            if (!screenshotFolder.mkdir()) {
                System.out.println("fail create folder");
            }
        }
        try {
            FileUtils.copyFileToDirectory(screenshot, screenshotFolder);
        } catch (IOException e) {
            LOG.error("Fail to save screenshot");
            System.out.println("Fail to save screenshot");
        }
    }

    private void handleCheckRow(DomElement grid, String value)
            throws WebActionException {
        try {
            WebElement gridElement = grid.getDomElement();
            List<WebElement> rowElements = populateElements(
                    gridElement,
                    By.xpath(XPATH_KENDO_ROW + "/"
                            + String.format(XPATH_SEARCH_BY_TEXT, value)));
            if (rowElements.isEmpty()) {
                throw new WebActionException("row detected = 0",
                        new NoSuchElementException("row detected = 0"));
            }

            for (WebElement element : rowElements) {
                WebElement checkButton = populateElement(
                        element,
                        By.xpath("../preceding-sibling::td[@role='gridcell']/descendant::*[@class='mr-checkbox']"));
                doClick(checkButton);
            }
        } catch (WebActionException e) {
            throw new WebActionException("Failed to check row " + value
                    + " at " + grid.getIdentifier(), e);
        }
    }

    /**
     * Perfoming upload at element
     * 
     * @param element
     * @param value
     * @throws WebActionException
     */
    private void handleUpload(DomElement element, String value)
            throws WebActionException {
        try {
            WebElement imageLayout = populateElement(element.getDomElement(),
                    By.xpath(".."));
            Actions hoverAction = new Actions(webDriver);
            hoverAction.moveToElement(imageLayout).perform();

            WebElement uploadButton = populateElement(imageLayout,
                    By.xpath("descendant::*[@class='mr-photo-info']"));
            if (!performClick(uploadButton)) {
                throw new WebDriverException("fail to click");
            } else {
                StringSelection selection = new StringSelection(value);
                Toolkit.getDefaultToolkit().getSystemClipboard()
                        .setContents(selection, null);
                try {

                    Robot robot = new Robot();
                    robot.keyPress(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_V);
                    robot.keyRelease(KeyEvent.VK_V);
                    robot.keyRelease(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_ENTER);

                    hoverAction.moveToElement(imageLayout).perform();
                    WebElement saveButton = populateElement(By
                            .xpath("//*[@id='scr-pat-layout-01-patient-upload-picture' and text() = 'Save']"));
                    if (!performClick(saveButton)) {
                        throw new WebDriverException("fail to click");
                    }
                } catch (AWTException e) {
                    throw new WebActionException("Fail to upload", e);
                }
            }
        } catch (WebActionException e) {
            throw new WebActionException("Failed to upload " + value + " to "
                    + element.getIdentifier(), e);
        }
    }

    /**
     * Perform Deletion on Appointment
     * 
     * @param element
     * @throws WebActionException
     */
    private void handleDelete(DomElement element) throws WebActionException {
        try {
            WebElement theElement = populateElement(element);
            doHover(theElement);
            WebElement deleteButton = populateElement(theElement,
                    By.xpath("../following-sibling::*[1]/a"));
            doClick(deleteButton);
        } catch (WebActionException e) {
            throw new WebActionException("Failed to delete "
                    + element.getIdentifier(), e);
        }
    }

    public void doHover(final WebElement element) {
        Actions actions = new Actions(webDriver);
        Wait<Actions> loader = new FluentWait<Actions>(actions)
                .ignoring(StaleElementReferenceException.class)
                
                .withTimeout((long) elementWaitTimeout, TimeUnit.SECONDS)
                .pollingEvery(elementPoolingTimeout, TimeUnit.MILLISECONDS);
        loader.until(new Function<Actions, Boolean>() {

            @Override
            public Boolean apply(Actions input) {
                input.moveToElement(element).perform();
                return true;
            }

        });
    }

    /**
     * Perfoming specifict action of Kendo UI Date Time element
     * 
     * @param domElement
     * @param value
     * @throws WebActionException
     */
    private void handleDateTimePick(DomElement domElement, String value)
            throws WebActionException {
        try {
            if (value.contains("/")) {
                performKendoDatePick(domElement.getDomElement(), value);
            }
            WebElement hourButton = populateElement(domElement.getDomElement(),
                    By.xpath("../descendant::*[contains(@class,'k-i-clock')]"));

            doClick(hourButton);

            WebElement hourOptions = populateElement(By.xpath("//ul[@id='"
                    + hourButton.getAttribute("aria-controls") + "']"));
            if (hourOptions.isDisplayed()) {
                String query = "";
                if (value.contains("/")) {
                    query = value.substring(value.indexOf(" ") + 1);
                } else {
                    query = value;
                }
                WebElement containerOptions = populateElement(hourOptions,
                        By.xpath(".."));
                List<WebElement> listOptions = populateElements(hourOptions,
                        By.xpath("li"));
                WebElement hourTarget = performScrolltoElement(
                        containerOptions, listOptions, query);
                if (hourTarget == null) {
                    throw new NoSuchElementException(
                            "Element in options not found");
                } else {
                    doClick(hourTarget);
                }
            } else {
                throw new NoSuchElementException("Options doesnt show");
            }
        } catch (WebDriverException | WebActionException e) {
            throw new WebActionException("Failed to pick " + value + " at "
                    + domElement.getIdentifier(), e);
        }
    }

    /**
     * Performing specific click action. Especially for Kendo UI element
     * 
     * @param element
     * @return
     */
    private boolean performClick(WebElement element) {
        boolean success = false;
        int retry = 0;
        int maxRetry = 10;
        int movement = 50;
        while (!success && retry < maxRetry) {
            try {
                doClick(element);
                success = true;
            } catch (Exception e) {
                WebElement container = populateElement(
                        element,
                        By.xpath("ancestor::*[contains(@class,'mCSB_container')]"));
                String topValue = container.getCssValue("top");
                int startPosition = Integer.valueOf(topValue.substring(0,
                        topValue.indexOf("p")));
                ((JavascriptExecutor) webDriver).executeScript("$('#"
                        + container.getAttribute("id") + "').css('top','"
                        + String.format("%dpx", startPosition - movement)
                        + "');");
            }
            retry++;
        }
        return success;
    }

    /**
     * Performing Expand Row for Kendo UI Tree
     * 
     * @param domElement
     * @param rowIdentifier
     * @throws WebDriverException
     * @throws WebActionException
     */
    private void handleKendoExpandRow(DomElement domElement,
            String rowIdentifier) throws WebActionException {
        try {
            WebElement gridElement = domElement.getDomElement();
            List<WebElement> rowElements = populateElements(
                    gridElement,
                    By.xpath(XPATH_KENDO_ROW
                            + "/"
                            + String.format(XPATH_SEARCH_BY_TEXT, rowIdentifier)));
            if (rowElements.size() == 0) {
                throw new NoSuchElementException("row detected = 0");
            }

            for (WebElement element : rowElements) {
                WebElement expandButton = populateElement(
                        element,
                        By.xpath("../preceding-sibling::td[@class='k-hierarchy-cell']"));
                doClick(expandButton);
            }
        } catch (WebActionException | WebDriverException e) {
            throw new WebActionException("Failed to expand row "
                    + rowIdentifier + " at " + domElement.getIdentifier(), e);
        }
    }

    /**
     * Performing action Kendo UI Date Element
     * 
     * @param webElement
     * @param value
     * @throws WebActionException
     */
    private void performKendoDatePick(WebElement webElement, String value)
            throws WebActionException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        WebElement calendarButton = populateElement(webElement,
                By.xpath("../descendant::*[contains(@class,'k-i-calendar')]"));

        doClick(calendarButton);

        String popupId = (calendarButton.getAttribute("aria-controls")) != null ? calendarButton
                .getAttribute("aria-controls") : webElement
                .getAttribute("aria-owns");

        WebElement datepickContainer = populateElement(By.xpath("//*[@id='"
                + popupId + "']"));
        try {
            Date date = dateFormat.parse(value);
            Calendar todayCalendar = Calendar.getInstance();
            Calendar targetCalendar = Calendar.getInstance();
            targetCalendar.setTime(date);

            int yearDiff = targetCalendar.get(Calendar.YEAR)
                    - todayCalendar.get(Calendar.YEAR);
            int monthDiff = targetCalendar.get(Calendar.MONTH)
                    - todayCalendar.get(Calendar.MONTH);
            monthDiff = monthDiff + (yearDiff * 12);
            WebElement monthControll;
            if (monthDiff > 0) {
                monthControll = populateElement(
                        datepickContainer,
                        By.xpath("descendant::*[contains(@class,'k-nav-next')]"));
            } else {
                monthControll = populateElement(
                        datepickContainer,
                        By.xpath("descendant::*[contains(@class,'k-nav-prev')]"));
            }
            for (int i = 0; i < Math.abs(monthDiff); i++) {
                doClick(monthControll);
            }

            int yearVal = targetCalendar.get(Calendar.YEAR);
            int monthVal = targetCalendar.get(Calendar.MONTH);
            int dayVal = targetCalendar.get(Calendar.DAY_OF_MONTH);

            String dateval = String.format("%d/%d/%d", yearVal, monthVal,
                    dayVal);
            WebElement dateTarget = populateElement(datepickContainer,
                    By.xpath("descendant::*[@data-value='" + dateval + "']"));

            doClick(dateTarget);
        } catch (ParseException e) {
            throw new WebActionException("Parse date error", e);
        } catch (WebActionException e) {
            throw new WebActionException("Failed to Pick " + value, e);
        }
    }

    /**
     * Performing Dropdown selection for Kendo UI Select element
     * 
     * @param domElement
     * @param selectValue
     * @throws NoSuchElementException
     * @throws WebActionException
     */
    private void performKendoSelect(DomElement domElement, String selectValue)
            throws WebActionException {
        try {
            doKendoSelect(domElement.getDomElement(), selectValue);
        } catch (WebDriverException | WebActionException e) {
            throw new WebActionException("Failed to select " + selectValue
                    + " at " + domElement.getIdentifier(), e);
        }
    }

    /**
     * Performing enter action. Handling {@link Keys} action too.
     * 
     * @param domElement
     * @param action
     */
    private void performEnterAction(WebElement element, String action) {
        if (action.startsWith("Keys.")) {
            String keys = action.replace("Keys.", "");
            doEnterAction(element, Keys.valueOf(keys));
        } else {
            doEnterAction(element, action);
        }
    }

    /**
     * Performing double click action
     * 
     * @param element
     * @throws WebActionException
     */
    private void performDoubleClick(WebElement element)
            throws WebActionException {
        if (element.isDisplayed()) {
            doDoubleClick(populateElement(element, self));
        } else {
            jsDriver.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
            jsDriver.executeScript("arguments[0].scrollIntoView();", element);
            doDoubleClick(populateElement(element, self));
        }
    }

    /**
     * Performing double click on {@link SchedulerCell} model for Appointment
     * Grid
     * 
     * @param schedule
     * @param grid
     * @throws WebActionException
     */
    protected void performDoubleClick(SchedulerCell schedule, String grid)
            throws WebActionException {
        try {
            String gridId = getDomElement(grid).getId();
            int row = getRowByTime(schedule.getTime(), gridId) + 1;
            int col = getColByDoctor(schedule.getDoctor(), gridId) + 1;
            if (row > 0 && col > 0) {
                String queryXpath = String.format(XPATH_APPOINTMENT_CELL,
                        gridId, row, col);
                WebElement cellElement = populateElement(By.xpath(queryXpath));
                // always give second chance.
                try {
                    if (!cellElement.isDisplayed()) {
                        jsDriver.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
                        jsDriver.executeScript(
                                "arguments[0].scrollIntoView();", cellElement);
                    }

                    doDoubleClick(cellElement);
                } catch (Exception enve) {
                    try {
                        doDoubleClick(cellElement);
                    } catch (ElementNotVisibleException enve2) {
                        if (!cellElement.isDisplayed()) {
                            jsDriver.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
                            jsDriver.executeScript(
                                    "arguments[0].scrollIntoView();",
                                    cellElement);
                        }
                        doDoubleClick(cellElement);
                    }
                }
            } else {
                throw new WebActionException("Invalid cell element");
            }
        } catch (WebActionException e) {
            throw new WebActionException("Failed to doubleclick at " + grid, e);
        }
    }

    /**
     * Performing scroll on Kendo UI Selection.
     * 
     * @param container
     * @param options
     * @param target
     * @return
     */
    private WebElement performScrolltoElement(WebElement container,
            List<WebElement> options, String target) {
        int movement = 90;
        for (WebElement element : options) {
            if (!element.isDisplayed()) {
                String topValue = container.getCssValue("top");
                int startPosition = Integer.valueOf(topValue.substring(0,
                        topValue.indexOf("p")));
                ((JavascriptExecutor) webDriver).executeScript("$('#"
                        + container.getAttribute("id") + "').css('top','"
                        + String.format("%dpx", startPosition - movement)
                        + "');");
            }
            element = populateElement(element, self);
            if (element.isDisplayed() && element.getText().contains(target)) {
                return element;
            }
        }
        return null;
    }

    /**
     * Generic Action of Kendo Selection
     * 
     * @param webElement
     * @param selectValue
     * @throws NoSuchElementException
     * @throws WebActionException
     */
    private void doKendoSelect(WebElement webElement, String selectValue)
            throws NoSuchElementException, WebActionException {
        WebElement dropdownField = populateElement(By
                .xpath("//descendant::input[@id='"
                        + webElement.getAttribute("id") + "']/.."));

        if (!performClick(dropdownField)) {
            throw new WebDriverException("click failed");
        }
        WebElement optionContainer = populateElement(By.xpath("//ul[@id='"
                + dropdownField.getAttribute("aria-owns") + "']"));

        if (optionContainer.isDisplayed()) {
            WebElement target = findElementByText(optionContainer, selectValue,
                    "li");
            if (target == null) {
                WebElement scrollableContainer = populateElement(
                        optionContainer, By.xpath(".."));
                List<WebElement> listOptions = populateElements(
                        optionContainer, By.xpath("li"));
                target = performScrolltoElement(scrollableContainer,
                        listOptions, selectValue);
            }
            doClick(target);
        } else {
            throw new WebActionException("not displayed. Failed to click!");
        }
    }

    protected void doClick(WebElement element) throws WebActionException {
        Wait<WebElement> loader = new FluentWait<WebElement>(element)
                .withTimeout((long) elementWaitTimeout, TimeUnit.SECONDS)
                .pollingEvery(elementPoolingTimeout, TimeUnit.MILLISECONDS)
                .ignoring(StaleElementReferenceException.class);
        loader.until(new Function<WebElement, Boolean>() {

            @Override
            public Boolean apply(WebElement input) {
                if (input.isDisplayed()) {
                    input.click();
                } else {
                    jsDriver.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
                    jsDriver.executeScript("arguments[0].scrollIntoView();",
                            input);
                    input.click();
                }
                return true;
            }
        });

    }

    /**
     * Handle special action of sendKeys
     * 
     * @param domElement
     *            element target
     * @param keys
     *            action keys
     */
    protected void doEnterAction(WebElement element, Keys keys) {
        if ((browser == Browsers.IE) || (browser == Browsers.Safari)) {
            element.click();
        }
        element.sendKeys(keys);
    }

    protected void doEnterAction(WebElement element, String keys) {
        try {
            element.clear();
        } catch (Exception e) {
            // do nothing for now.
        }
        if ((browser == Browsers.IE) || (browser == Browsers.Safari)) {
            element.click();
        }
        element.sendKeys(keys);
    }

    /**
     * Generic Action of Double click
     * 
     * @param element
     * @throws WebActionException
     */
    private void doDoubleClick(WebElement element) throws WebActionException {
        Wait<WebElement> loader = new FluentWait<WebElement>(element)
                .withTimeout((long) elementWaitTimeout, TimeUnit.SECONDS)
                .pollingEvery(elementPoolingTimeout, TimeUnit.MILLISECONDS)
                .ignoring(StaleElementReferenceException.class);
        loader.until(new Function<WebElement, Boolean>() {

            @Override
            public Boolean apply(WebElement input) {
                Actions webActions = new Actions(webDriver);
                webActions.doubleClick(input).build().perform();
                return true;
            }
        });
    }

    /**
     * Accept alert if present. To avoid error caused by alert
     */
    private void acceptAlertifPresent() {
        try {
            Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver)
                    .withTimeout(elementWaitTimeout, TimeUnit.SECONDS);
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = webDriver.switchTo().alert();
            alert.accept();
        } catch (Exception e) {

        }
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException}
     * 
     * @param by
     * @return
     */
    private List<WebElement> populateElements(By by) {
        return populateElements(null, by);
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException} with
     * root element
     * 
     * @param element
     *            root of element
     * @param by
     *            query of element based on root
     * @return
     */
    private List<WebElement> populateElements(final WebElement element, By by) {
        Wait<By> loader = new FluentWait<By>(by)
                .withTimeout((long) elementWaitTimeout, TimeUnit.SECONDS)
                .pollingEvery(elementPoolingTimeout, TimeUnit.MILLISECONDS)
                .ignoring(StaleElementReferenceException.class);
        return loader.until(new Function<By, List<WebElement>>() {

            @Override
            public List<WebElement> apply(By input) {
                return (element != null) ? element.findElements(input)
                        : webDriver.findElements(input);
            }

        });
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException}
     * 
     * @param by
     * @return
     */
    private WebElement populateElement(By by) {
        return populateElement(null, by);
    }

    private WebElement populateElement(DomElement element) {
        if (element.getId() != null) {
            return populateElement(By.id(element.getId()));
        } else if (element.getName() != null) {
            return populateElement(By.name(element.getName()));
        } else if (element.getLinkText() != null) {
            return populateElement(By.linkText(element.getLinkText()));
        } else if (element.getClassName() != null) {
            return populateElement(By.className(element.getClassName()));
        } else if (element.getXpath() != null) {
            return populateElement(By.xpath(element.getXpath()));
        } else if (element.getCssSelector() != null) {
            return populateElement(By.cssSelector(element.getCssSelector()));
        } else if (element.getPartialLinkText() != null) {
            return populateElement(By.partialLinkText(element
                    .getPartialLinkText()));
        } else if (element.getTagName() != null) {
            return populateElement(By.tagName(element.getTagName()));
        } else {
            return null;
        }
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException} with
     * root element
     * 
     * @param element
     *            element root of element
     * @param by
     *            query of element based on root
     * @return
     */
    private WebElement populateElement(final WebElement element, By by) {
        Wait<By> loader = new FluentWait<By>(by)
                .ignoring(StaleElementReferenceException.class)
                .withTimeout((long) elementWaitTimeout, TimeUnit.SECONDS)
                .pollingEvery(elementPoolingTimeout, TimeUnit.MILLISECONDS);
        return loader.until(new Function<By, WebElement>() {

            @Override
            public WebElement apply(By input) {
                return (element != null) ? element.findElement(input)
                        : webDriver.findElement(input);
            }

        });
    }

    /**
     * Find an Element based on Text containing. If multiple element is
     * selected, the first element will be return
     * 
     * @param root
     * @param text
     * @param tagTarget
     * @return
     * @throws NoSuchElementException
     */
    private WebElement findElementByText(WebElement root, String text,
            String tagTarget) throws NoSuchElementException {
        List<WebElement> listofelements = root.findElements(By.xpath(String
                .format(XPATH_SEARCH_BY_TEXT, text)));
        for (WebElement element : listofelements) {
            if (element.isDisplayed() && element.isEnabled()) {
                if (tagTarget != null && element.getTagName().equals(tagTarget)) {
                    return element;
                } else {
                    return element;
                }
            }
        }
        return null;
    }

    /**
     * Get an text from element. if Element is'n visible it will be scrolled to
     * element
     * 
     * @param element
     * @return
     */
    private String getText(WebElement element) {
        String result = element.getText();
        if (!(result.trim().length() > 0)) {
            jsDriver.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
            jsDriver.executeScript("arguments[0].scrollIntoView();", element);
            result = populateElement(element, self).getText();
        }
        return result;
    }

    /**
     * Get row number of Appointment Grid
     * 
     * @param time
     * @param grid
     * @return
     */
    private int getRowByTime(String time, String grid) {
        List<WebElement> hourLabels = populateElements(By.xpath(String.format(
                XPATH_APPOINTMENT_SCHEDULER_TIME, grid)));
        int result = -1;
        for (int i = 0; i < hourLabels.size() && result == -1; i++) {
            if (getText(hourLabels.get(i)).contains(time)) {
                result = i;
            }
        }
        return result;
    }

    /**
     * Get Column number of Appointment Grid
     * 
     * @param doctor
     * @param grid
     * @return
     */
    private int getColByDoctor(String doctor, String grid) {
        List<WebElement> doctorLabels = populateElements(By.xpath(String
                .format(XPATH_APPOINTMENT_DOCTORS, grid)));
        int result = -1;
        for (int i = 0; i < doctorLabels.size() && result == -1; i++) {
            if (getText(doctorLabels.get(i)).contains(doctor)) {
                result = i;
            }
        }
        return result;
    }

    public ScreenshotMode getScreenshotMode() {
        return screenshotMode;
    }

    public void setScreenshotMode(ScreenshotMode screenshotMode) {
        this.screenshotMode = screenshotMode;
    }

}
