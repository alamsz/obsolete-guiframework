package com.mitrais.guitestautomation.bridgetest.model;

public class SchedulerCell {
    private String time;
    private String doctor;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

}
