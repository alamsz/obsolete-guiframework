package com.mitrais.guitestautomation.bridgetest;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "examples/MRAPP/features/04appointment.feature" }, plugin = {
        "pretty", "html:target/cucumber-html-report",
        "json:target/cucumber-report.json" })
public class BridgeRunner {
}
