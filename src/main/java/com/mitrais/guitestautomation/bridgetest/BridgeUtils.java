package com.mitrais.guitestautomation.bridgetest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.mitrais.guitestautomation.WebUIManager;
import com.mitrais.guitestautomation.exception.IllegalCucumberFormatException;
import com.mitrais.guitestautomation.page.DomElementExpectation;
import com.mitrais.guitestautomation.page.HtmlActionConditionEnum;
import com.mitrais.guitestautomation.page.Page;
import com.mitrais.guitestautomation.util.Constants;

import cucumber.api.DataTable;

/**
 * Contains many supporting method to map jazz functionality to cucumber
 * 
 * @author Aldira_R
 *
 */
public class BridgeUtils {

    private BridgeUtils() {
    }

    /**
     * Generate Expect object to be evaluated.
     * 
     * @param dataTable
     * @return
     * @throws IllegalCucumberFormatException
     */
    public static List<DomElementExpectation> expectBridge(DataTable dataTable)
            throws IllegalCucumberFormatException {
        List<DomElementExpectation> expectations = new ArrayList<DomElementExpectation>();
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        for (String dataKey : data.keySet()) {
            String expectation = data.get(dataKey);
            DomElementExpectation expect = new DomElementExpectation();

            expect.setComponentName(normalizeIdentifier(dataKey));
            // Jazz
            if (expectation.equalsIgnoreCase(HtmlActionConditionEnum.VISIBLE
                    .getValue())) {
                expect.setCondition(HtmlActionConditionEnum.VISIBLE.getValue());
            } else if (expectation
                    .equalsIgnoreCase(HtmlActionConditionEnum.INVISIBLE
                            .getValue())) {
                expect.setCondition(HtmlActionConditionEnum.INVISIBLE
                        .getValue());
            } else {
                expect.setCondition(HtmlActionConditionEnum.EQUALS.getValue());
                expect.setValue(expectation);
            }
            expectations.add(expect);
        }
        return expectations;
    }

    /**
     * What does return true mean? False? //CODEREVIEW - need docs
     *
     * @param scenario
     * @param scenarioResult
     * @param page
     *
     * @return
     */

    public static void pageSetup(WebDriver driver, JavascriptExecutor jsDriver,
            Page page) {
        page.setWebDriver(driver);
        page.setJsDriver(jsDriver);

        WebUIManager webUiManager = WebUIManager.getInstance();

        WebUIManager.loadJQuery(jsDriver);
        page.setPageLoadTimeout(WebUIManager.getPageLoadTimeout());
        page.setActionPace(WebUIManager.getActionPace());
        page.setElementPace(WebUIManager.getElementPace());
        page.setElementWaitTimeout(WebUIManager.getElementWaitTimeout());
        page.setElementPoolingTimeout(WebUIManager.getElementPoolingTimeout());
        page.setBrowser(webUiManager.getBrowser());
        ((PageBridge) page).setScreenshotMode(webUiManager.getScreenshotMode());
    }

    public static String normalizeIdentifier(String identifier) {
        if (identifier.contains(".")) {
            return identifier.substring(identifier.indexOf(".") + 1);
        } else {
            return identifier;
        }
    }
}
