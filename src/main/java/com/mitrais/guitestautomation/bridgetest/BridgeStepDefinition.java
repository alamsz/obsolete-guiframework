package com.mitrais.guitestautomation.bridgetest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;

import org.apache.regexp.RE;
import org.openqa.selenium.WebDriver;

import com.mitrais.guitestautomation.WebUIManager;
import com.mitrais.guitestautomation.action.HtmlAction;
import com.mitrais.guitestautomation.action.HtmlActionStatus;
import com.mitrais.guitestautomation.bridgetest.model.SchedulerCell;
import com.mitrais.guitestautomation.exception.IllegalCucumberFormatException;
import com.mitrais.guitestautomation.exception.WebActionException;
import com.mitrais.guitestautomation.page.DomElementExpectation;
import com.mitrais.guitestautomation.page.Page;
import com.mitrais.guitestautomation.processor.ActionRequest;
import com.mitrais.guitestautomation.processor.KendoProcessor;
import com.mitrais.guitestautomation.processor.PageProcessor;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BridgeStepDefinition {

    private WebUIManager testManager = WebUIManager.getInstance();
    private static WebDriver testDriver;
    private PageBridge testPage;

    private static PageProcessor processor = KendoProcessor.getInstance();

    // @Given("^the following settings:$")
    // public void the_following_settings(DataTable dataBackgroud)
    // throws Throwable {
    // }

    /**
     * Checking if browser is available or not
     * 
     * @throws MalformedURLException
     */
    private void initBrowser() throws MalformedURLException {
        if (testDriver == null) {
            testDriver = testManager.getWebDriver();
            testDriver
                    .manage()
                    .timeouts()
                    .implicitlyWait(WebUIManager.getActionPace(),
                            TimeUnit.SECONDS);
            testDriver.get(testManager.getBaseUrl());
            testDriver.manage().window().maximize();
        }
    }

    @Given("^I have logged in using$")
    public void loggedInUsing(DataTable arg1) throws MalformedURLException,
            WebActionException {
        iamOn("LoginPage");
        enter(arg1);
        click("loginButton");
    }

    @After("@last")
    public void cleanUp() throws InterruptedException, WebActionException {
        processor.terminateProcessor();
    }

    @Before("@login")
    public void loginIn() throws WebActionException, MalformedURLException {
        iamOn("LoginPage");
        Map<String, String> dataEntry = new HashMap<String, String>();
        dataEntry.put("username", "olivia.charlotte");
        dataEntry.put("email", "oliviacharlotte.doctor@gmail.com");
        dataEntry.put("password", "password");
        enterAction(dataEntry, testPage);
        click("loginButton");
    }

    @Given("^I am ON \"(.*?)\"$")
    public void iamOn(String arg1) throws WebActionException {
        processor.setup(arg1);
        assertTrue("no exception on setup", true);
    }

    @Then("^I should EXPECT$")
    public void shouldExpect(DataTable data) throws WebActionException,
            IllegalCucumberFormatException {
        for (DomElementExpectation elementExpectation : BridgeUtils
                .expectBridge(data)) {
            assertTrue(processor.doExpect(elementExpectation));
        }
    }

    @Given("^I have hovered \"(.*?)\"$")
    @When("^I hover \"(.*?)\"$")
    public void i_hover(String identifier) throws Throwable {
        ActionRequest request = new ActionRequest(HtmlAction.HOVER, identifier);
        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
    }

    @Given("^I have clicked \"(.*?)\"$")
    @When("^I click \"(.*?)\"$")
    public void click(String identifier) throws WebActionException {
        ActionRequest request = new ActionRequest(HtmlAction.CLICK,
                BridgeUtils.normalizeIdentifier(identifier));
        HtmlActionStatus status = processor.doAction(request);
        assertEquals(HtmlActionStatus.GOOD, status);
    }

    @Given("^I have entered$")
    @When("^I enter$")
    public void enter(DataTable data) throws WebActionException {
        Map<String, String> dataEntry = data.asMap(String.class, String.class);
        enterAction(dataEntry, testPage);
    }

    @Given("^I have double-clicked on \"(.*?)\"$")
    @When("^I double-click on \"(.*?)\"$")
    public void doubleClickOn(String arg1, DataTable arg2)
            throws WebActionException {
        // TODO change to processor
        List<SchedulerCell> cells = arg2.asList(SchedulerCell.class);
        HtmlActionStatus status = testPage.executeWebAction(cells.get(0),
                HtmlAction.DOUBLE_CLICK, BridgeUtils.normalizeIdentifier(arg1));
        assertEquals(HtmlActionStatus.GOOD, status);
    }

    private void enterAction(Map<String, String> dataEntry, Page testPage)
            throws WebActionException {
        for (String identifier : dataEntry.keySet()) {
            ActionRequest request = new ActionRequest(HtmlAction.ENTER,
                    BridgeUtils.normalizeIdentifier(identifier));
            request.addParameters(dataEntry.get(identifier));
            HtmlActionStatus status = processor.doAction(request);
            assertEquals(HtmlActionStatus.GOOD, status);
        }
    }

    @Then("^I should be ON \"(.*?)\"$")
    public void shouldBeOn(String arg1) throws WebActionException,
            MalformedURLException {
        processor.setup(arg1);
        assertTrue("no exception on setup", true);
    }

    @Given("^I have selected$")
    @When("^I select$")
    public void select(DataTable data) throws WebActionException {
        // TODO change to processor
        Map<String, String> dataEntry = data.asMap(String.class, String.class);
        for (String identifier : dataEntry.keySet()) {
            HtmlActionStatus status = testPage.executeWebAction(
                    testPage.getDomElement(BridgeUtils
                            .normalizeIdentifier(identifier)),
                    HtmlAction.SELECT, dataEntry.get(identifier));
            assertEquals(HtmlActionStatus.GOOD, status);
        }
    }

    @Given("^I have waited (\\d+) seconds$")
    @When("^I wait (\\d+) seconds$")
    public void waitSeconds(int arg1) throws WebActionException {
        ActionRequest request = new ActionRequest(HtmlAction.WAIT, null);
        request.addParameters(arg1);
        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
    }

    @Given("^I have selected dropdown$")
    @When("^I select dropdown$")
    public void selectDropdown(DataTable data) throws WebActionException {
        Map<String, String> dataEntry = data.asMap(String.class, String.class);
        for (String identifier : dataEntry.keySet()) {
            ActionRequest request = new ActionRequest(
                    HtmlAction.KENDO_DROPDOWN,
                    BridgeUtils.normalizeIdentifier(identifier));
            request.addParameters(dataEntry.get(identifier));
            assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
        }
    }

    /**
     * Expand one of row from grid
     * 
     * @param rowIdentifier
     *            identifier of row. Can be one of value from row
     * @param gridIdentifier
     *            the grid that contain selected row
     * @throws WebActionException
     *             if row doesn't have expand button (not expandable)
     */
    @Given("^I have expanded \"(.*?)\" at \"(.*?)\"$")
    @When("^I expand \"(.*?)\" at \"(.*?)\"$")
    public void expandAt(String rowIdentifier, String gridIdentifier)
            throws WebActionException {
        ActionRequest request = new ActionRequest(HtmlAction.KENDO_EXPAND_ROW,
                gridIdentifier);
        request.addParameters(rowIdentifier);
        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
    }

    @Given("^I have refreshed page$")
    @When("^I refresh page$")
    public void refreshPage() throws WebActionException {
        ActionRequest request = new ActionRequest(HtmlAction.REFRESH, null);
        assertEquals(HtmlActionStatus.GOOD, request);
    }

    @Given("^I have dragged \"(.*?)\" to \"(.*?)\"$")
    @When("^I drag \"(.*?)\" to \"(.*?)\"$")
    public void dragTo(String origin, String destination)
            throws WebActionException {
        ActionRequest request = new ActionRequest(HtmlAction.DRAG,
                BridgeUtils.normalizeIdentifier(origin));
        request.addParameters(BridgeUtils.normalizeIdentifier(destination));
        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
    }

    @Given("^I have double-clicked ON \"(.*?)\"$")
    @When("^I double-click ON \"(.*?)\"$")
    public void doubleClickOn(String identifier) throws WebActionException {
        ActionRequest request = new ActionRequest(HtmlAction.DOUBLE_CLICK,
                BridgeUtils.normalizeIdentifier(identifier));
        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
    }

    @Given("^I have picked datetime$")
    @When("^I pick datetime$")
    public void pickDateTime(DataTable arg1) throws WebActionException {
        Map<String, String> dataTable = arg1.asMap(String.class, String.class);
        for (String key : dataTable.keySet()) {
            ActionRequest request = new ActionRequest(
                    HtmlAction.KENDO_DATETIME,
                    BridgeUtils.normalizeIdentifier(key));
            request.addParameters(dataTable.get(key));
            assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
        }
    }

    @Given("^I have deleted \"(.*?)\"$")
    @When("^I delete \"(.*?)\"$")
    public void delete(String identifier) throws WebActionException {
        ActionRequest request = new ActionRequest(HtmlAction.DELETE,
                BridgeUtils.normalizeIdentifier(identifier));
        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
    }

    @Given("^I have picked date$")
    @When("^I pick date$")
    public void pickDate(DataTable arg1) throws WebActionException {
        Map<String, String> dataTable = arg1.asMap(String.class, String.class);
        for (String key : dataTable.keySet()) {
            ActionRequest request = new ActionRequest(HtmlAction.KENDO_DATE,
                    BridgeUtils.normalizeIdentifier(key));
            request.addParameters(dataTable.get(key));
            assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
        }
    }

    @Given("^I have uploaded$")
    @When("^I upload$")
    public void upload(DataTable arg1) throws WebActionException {
        // TODO change to processor
        Map<String, String> dataTable = arg1.asMap(String.class, String.class);
        for (String key : dataTable.keySet()) {
            HtmlActionStatus status = testPage.executeWebAction(testPage
                    .getDomElement(BridgeUtils.normalizeIdentifier(key)),
                    HtmlAction.UPLOAD, dataTable.get(key));
            assertEquals(HtmlActionStatus.GOOD, status);
        }
    }

    @Given("^I have checked \"(.*?)\" at \"(.*?)\"$")
    @When("^I check \"(.*?)\" at \"(.*?)\"$")
    public void checkAt(String arg1, String arg2) throws WebActionException {
        // TODO change to processor
        HtmlActionStatus status = testPage.executeWebAction(
                testPage.getDomElement(BridgeUtils.normalizeIdentifier(arg2)),
                HtmlAction.CHECK, arg1);
        assertEquals(HtmlActionStatus.GOOD, status);
    }

}
